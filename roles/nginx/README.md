Nginx installation role for Red Hat Enterprise Linux and its derivatives such as CentOS, Oracle Linux, Rocky Linux, AlmaLinux.
=========
This role installs the nginx package, starts and enables it, and adds the site configuration file.

Role variables:
--------------
`servername` - name of the site in the config file \
`root_directory` - directory where site files are located

Example playbook:
----------------
```
---
- name: nginx server setup
  hosts: all
  become: true
  roles:
    - name: nginx
```